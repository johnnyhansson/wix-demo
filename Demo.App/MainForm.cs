﻿using System;
using System.Windows.Forms;

namespace Demo.App
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void OnCloseClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
